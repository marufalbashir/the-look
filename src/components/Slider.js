import React,{Component} from 'react'

class Slider extends Component{
    render(){
        return(
            <div>
                <div className="hs-item">
                    <div className="hs-bg set-bg sm-overlay" data-setbg="%PUBLIC_URL%/assets/img/slider/1.jpg"></div>
                    <div className="sp-container">
                        <div className="hs-text">
                            <h2>The Look<br/>Gallery</h2>
                            <p>John Doe  Showcase<br/>23 January - 14 February</p>
                            <a href="#" className="site-btn sb-big">Read More 
                            <img src="%PUBLIC_URL%/assets/img/icons/arrow-right-black.png" alt=""></img>

                            </a>
                        </div>
                    </div>
                </div>
                <div className="hs-item">
                    <div className="hs-bg set-bg sm-overlay" data-setbg="%PUBLIC_URL%/assets/img/slider/2.jpg"></div>
                    <div className="sp-container">
                        <div className="hs-text">
                            <h2>The Look<br/>Gallery</h2>
                            <p>John Doe  Showcase<br/>23 January - 14 February</p>
                            <a href="#" className="site-btn sb-big">Read More 
                            <img src="%PUBLIC_URL%/assets/img/icons/arrow-right-black.png" alt=""></img></a>
                        </div>
                    </div>
                </div>
                <div className="hs-item">
                    <div className="hs-bg set-bg sm-overlay" data-setbg="%PUBLIC_URL%/assets/img/slider/3.jpg"></div>
                    <div className="sp-container">
                        <div className="hs-text">
                            <h2>The Look<br/>Gallery</h2>
                            <p>John Doe  Showcase<br/>23 January - 14 February</p>
                            <a href="#" className="site-btn sb-big">Read More 
                            <img src="%PUBLIC_URL%/assets/img/icons/arrow-right-black.png" alt=""></img></a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
} 
export default Slider;