import React,{Component} from 'react';
import axios from 'axios';
class Blogs extends Component{
    state={
        blogs:[],
    }
    componentDidMount(){
		axios.get('https://marufcse.com/api/blogs').then(res=>this.setState({
			blogs:res.data,
		}	
		)
		)
		
    }
    render(){
        return(
            <React.Fragment>
                
                {
                this.state.blogs.map((blog)=>(
                    <div key={blog.id} className="col-md-3">
                        <div className="gallery-item text-center">
                            <div className="gallery-photo">
                                <img src={blog.image} alt="#"/>
                            </div>
                            <h4>{blog.title}</h4>                                
                            <p>Artist: {blog.user.name}</p>
                            <a href="/" className="site-btn"> {blog.view_count} Likes</a>
                        </div>
                    </div>
                
                
                )) 
                }

                
            </React.Fragment>
        )
            
        
    }
}
export default Blogs