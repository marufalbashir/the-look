import React, { Component } from 'react'
import axios from 'axios'
//import downloadIcon from './download.svg'
export default class Photo extends Component {

    state = {
        photo: [],
       // loading: true,
    }

    componentDidMount() {
        let search = window.location.search;
        let params = new URLSearchParams(search);
        let photo_id = params.get('id');

        axios.get('https://api.unsplash.com/photos/' + photo_id + '/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586').then(
            res => this.setState({
                photo: res.data,
                
            })
        )
        
    }

    render() {
        console.log(this.state.photo);
        var photo = this.state.photo
        return (
            <div>
                
                
                <div className="photo-single-wrapper">
                    <div className="photo-single-info">
                    {photo.description ? <h5 className="text-center">{photo.description}</h5> : ''}

                        <ul>
                            <li><label htmlFor="uplaodedd_by">Uploaded by</label> {photo.user && photo.user.first_name} {photo.user && photo.user.last_name}</li>
                            
                            {photo.updated_at ? <li><label htmlFor="upload_date">Upload date</label> {photo.updated_at}</li> : ''}
                            
                            <li><label htmlFor="camera_model">Camera model</label> {photo.exif && photo.exif.model}</li>
                            
                        </ul>

                        <a  rel="noopener noreferrer" href={photo.links && photo.links.download} download>Download </a>
                    </div>
                    <div className="photo-img-n"><img src={photo.urls && photo.urls.regular} alt=""/> </div>
                </div>
            </div>
        )
    }
}
