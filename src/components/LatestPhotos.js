import React,{Component} from 'react';
import axios from 'axios';
import {BrowserRouter as Router, Route} from 'react-router-dom'
//import Loading from 'react-loading-bar'
//import 'react-loading-bar/dist/index.css'
class LatestPhotos extends Component{
	state={
		photos:[],
		page: 1,
		show: true,
		searchQuery: '',
		searching: false,
		searchQty:false,
	}
	onShow = ()=> {
		this.setState({ show: true })
	  }
	
	  onHide = ()=> {
		this.setState({ show: false })
	  }
	
	componentDidMount(){
		axios.get('https://api.unsplash.com/photos/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586&per_page=52&page='+ this.state.page).then(res=>this.setState({
			photos:res.data,
			show: false,
			total_found: false,
			page: 2,
		}	
		)
		)
		
	}
	
	loadNextPage = (e) =>{
		this.setState({page: this.state.page + 1});
		axios.get('https://api.unsplash.com/photos/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586&per_page=52&page='+ this.state.page).then(res=>this.setState({
			photos:res.data,
			show: false
		})
		)
		window.scrollTo(0, 0);
	}
	/*
	loadSearchNextPage = (e) =>{
		this.setState({
            show: true
        })
        axios.get('https://api.unsplash.com/search/photos/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586&query=' + this.state.searchQuery + '&per_page=50&page='+ this.state.page).then(
            res => this.setState({
                photos: res.data.results,
				show: false,
				searchQty: 11,
                total_found: res.data.total,
            })
        )
		window.scrollTo(0, 0);
	} */
	searchQuery=(e)=>{
		this.setState({
			searchQuery: e.target.value,
		});
		
	}
	searchTriger=(e)=>{
		this.setState({
            show: true
        })
        axios.get('https://api.unsplash.com/search/photos/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586&query=' + this.state.searchQuery + '&per_page=52&page='+ this.state.page).then(
            res => this.setState({
                photos: res.data.results,
				show: false,
				searchQty: 11,
                total_found: res.data.total,
            })
        )
		e.preventDefault();
	}
	SearchNextPage = (e) =>{
		this.setState({page: this.state.page + 1});
		axios.get('https://api.unsplash.com/search/photos/?client_id=558c85ac01253cdd6c583e81336d76b9a2bd4699c8fe0e3fea59d86e3460b586&query=' + this.state.searchQuery + '&per_page=52&page='+ this.state.page).then(
            res => this.setState({
                photos: res.data.results,
				show: true,
				total_found: res.data.total,			
            })
        )
		window.scrollTo(0, 0);
		e.preventDefault();
	}
	
    render(){
		var sQuery='';
		var sTotal='';
		var NextBtnMarkup='';
		if(this.state.searchQuery){
			sQuery=<strong>You Search for <i calss="text-success">{this.state.searchQuery}</i></strong>;
			sTotal=<strong>Total {this.state.total_found} results found</strong>;
			NextBtnMarkup=<button className="btn btn-lg btn-success mb-5" onClick={this.SearchNextPage}>Result  {this.state.page+1}>></button>;
		}
		else{
			sQuery='';
			sTotal='';
			NextBtnMarkup=<button className="btn btn-lg btn-success mb-5" onClick={this.loadNextPage}>Go Page {this.state.page+1}>></button>;
		}
		
		
		return(
			
			<React.Fragment>
				{console.log(this.state.page)}
				{console.log(this.state.photos)}
				
				{
					<div className="col-md-12 mb-5">
						 
							<form className="row" onSubmit={this.searchTriger}>
								<div className="col-lg-9 text-left">
									{sQuery} {sTotal}
								</div>
								<div className="col-md-3 form-inline">
								<input type="text" name="query" value={this.state.searchQuery} onChange={this.searchQuery} placeholder="Enter Keyword" className="form-control" onKeyUp={this.searchTriger}/>
								
									<input type="submit" value="Search" className="btn btn-md btn-success"/>
								</div>
							</form>
							
						
					 </div>
					
				}

				{
					this.state.photos.map((photo)=>(
						<div key={photo.id} className="col-md-3 col-lg-3 col-sm-6 col-xs-12">
							<div className="gallery-item text-left">
								<div className="gallery-photo">
									<a href={'photo?id='+photo.id}>
									<img src={photo.urls.small} alt="#"/>
									</a>
									
								</div>
								
								
								<p>Artist: {photo.user.name}</p>
								<a href="/" className="site-btn"> {photo.likes} Likes</a>
							</div>
						</div>
								
							
						
						
					)) 
				}
				
				<div className="col-md-12 mx-auto text-center">
					{NextBtnMarkup}
				</div>
				

			</React.Fragment>
		)
			
		
		console.log(this.state.photos);
			
			      
    }
}
export default LatestPhotos;