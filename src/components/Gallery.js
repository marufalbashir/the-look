import React,{Component} from 'react';

class Gallery extends Component{
    render(){
        return(
            <div>
                <section className="blog-section">
                    <div className="sp-container">
                        <div className="blog-title-col">
                            <h2>Latest from the blog</h2>
                        </div>
                        <div className="blog-content-col">
                            <div className="blog-item">
                                <div className="blog-thumb">
                                    <img src="./assets/img/blog/1_thumb.jpg" alt=""/>
                                </div>
                                <div className="blog-content">
                                    <span className="blog-cata">photography</span>
                                    <h4>How to take the perfect shot</h4>
                                    <span>January 23, 2019</span>
                                    <p>Pellentesque dictum nisl in nibh dictum volutpat nec a quam. Vivamus sus-cipit nisl quis nulla pretium, vitae ornare leo sollicitudin. Aenean quis velit pulvinar, pellentesque neque vel, laoreet orci. Suspendisse potenti. </p>
                                    <a href="#" className="site-btn">Read More <img src="/.assets/img/icons/arrow-right-black.png" alt=""/></a>
                                </div>
                            </div>
                            <div className="blog-item">
                                <div className="blog-thumb">
                                    <img src="./assets/img/blog/2_thumb.jpg" alt=""/>
                                </div>
                                <div className="blog-content">
                                    <span className="blog-cata">photography</span>
                                    <h4>10 tips for a new photographer</h4>
                                    <span>January 23, 2019</span>
                                    <p>Pellentesque dictum nisl in nibh dictum volutpat nec a quam. Vivamus sus-cipit nisl quis nulla pretium, vitae ornare leo sollicitudin. Aenean quis velit pulvinar, pellentesque neque vel, laoreet orci. Suspendisse potenti. </p>
                                    <a href="#" className="site-btn">Read More <img src="/.assets/img/icons/arrow-right-black.png" alt=""/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );

    }
}

export default Gallery;