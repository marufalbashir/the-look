
import React,{Component} from 'react';

class Footer extends Component{
    
    render(){
        return(
            <div>
                <footer className="footer-section spad">
                    <div className="sp-container">
                        <div className="row m-0">
                            <div className="col-lg-4 footer-text">
                                <h2>Get in touch</h2>
                                <p>Pellentesque dictum nisl in nibh dictum volutpat nec a quam. Vivamus suscipit nisl quis nulla pretium, vitae ornare leo sollic itudin. Aenean quis velit pulvinar, pellentesque neque vel, laoreet orci. Suspendisse potenti. </p>
                            </div>
                            <div className="col-lg-8">
                                <form className="contact-form">
                                    <div className="row">
                                        <div className="col-lg-4">
                                            <input type="text" placeholder="Your Name"></input>
                                        </div>
                                        <div className="col-lg-4">
                                            <input type="text" placeholder="Your Email"></input>
                                        </div>
                                        <div className="col-lg-4">
                                            <input type="text" placeholder="Subject"></input>
                                        </div>
                                        <div className="col-lg-12">
                                            <textarea placeholder="Message"></textarea>
                                            <button className="site-btn sb-light" type="submit">send message
                                             <img src="../assetsimg/icons/arrow-right-white.png" alt=""></img>

                                             </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="copyright">
                            {document.write(new Date().getFullYear())}  All rights reserved | &copy; 
                            <i className="fa fa-heart-o" aria-hidden="true"></i> by 
                            <a href="https://marufcse.com">maruf</a>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
    
}
export default Footer;