import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
//import logo from './logo.svg';
import Header from './components/Header';
import Footer from './components/Footer';
import LatestPhotos from './components/LatestPhotos'
//import Gallery from './components/Gallery'
//import Slider from './components/Slider'
import Photo from './components/Photo'
import About from './components/pages/About'
//import css and js

import './assets/css/bootstrap.min.css';
//import './assets/css/font_awesome.min.css';
//import './assets/css/slicknav.min.css';
import './assets/css/style.css';
// end import
import './App.css';
function App() {
  return (
    <Router>
    <div className="App">
 
      <Header/>
      <section className="gallery-section">
							<div className="container-fluid">
								<div className="row m-0">
                <Route exact path="/" render={props => (
                <LatestPhotos/>
              )}/>
								</div>
							</div>
							  
						</section>
        <Route path="/about" component={About}/>
        <Route path="/photo" component={Photo}/>
      <Footer/>
    </div>
     </Router>
  );
}

export default App;
